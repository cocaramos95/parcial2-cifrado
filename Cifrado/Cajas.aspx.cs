﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsFilesCloud;

namespace Cifrado
{
    public partial class Cajas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			Getclave();

		}
		private void Getclave()
        {
			string Sql = "SELECT CAJA FROM CLAVES";
			clave.Value = Conexion.RegSql(Sql);
		}

		private static int[] GetShiftIndexes(string key)
		{
			int keyLength = key.Length;
			int[] indexes = new int[keyLength];
			List<KeyValuePair<int, char>> sortedKey = new List<KeyValuePair<int, char>>();
			int i;

			for (i = 0; i < keyLength; ++i)
				sortedKey.Add(new KeyValuePair<int, char>(i, key[i]));

			sortedKey.Sort(
				delegate (KeyValuePair<int, char> pair1, KeyValuePair<int, char> pair2) {
					return pair1.Value.CompareTo(pair2.Value);
				}
			);

			for (i = 0; i < keyLength; ++i)
				indexes[sortedKey[i].Key] = i;

			return indexes;
		}

		public static string Encipher(string input, string key, char padChar)
		{
			input = (input.Length % key.Length == 0) ? input : input.PadRight(input.Length - (input.Length % key.Length) + key.Length, padChar);
			StringBuilder output = new StringBuilder();
			int totalChars = input.Length;
			int totalColumns = key.Length;
			int totalRows = (int)Math.Ceiling((double)totalChars / totalColumns);
			char[,] rowChars = new char[totalRows, totalColumns];
			char[,] colChars = new char[totalColumns, totalRows];
			char[,] sortedColChars = new char[totalColumns, totalRows];
			int currentRow, currentColumn, i, j;
			int[] shiftIndexes = GetShiftIndexes(key);

			for (i = 0; i < totalChars; ++i)
			{
				currentRow = i / totalColumns;
				currentColumn = i % totalColumns;
				rowChars[currentRow, currentColumn] = input[i];
			}

			for (i = 0; i < totalRows; ++i)
				for (j = 0; j < totalColumns; ++j)
					colChars[j, i] = rowChars[i, j];

			for (i = 0; i < totalColumns; ++i)
				for (j = 0; j < totalRows; ++j)
					sortedColChars[shiftIndexes[i], j] = colChars[i, j];

			for (i = 0; i < totalChars; ++i)
			{
				currentRow = i / totalRows;
				currentColumn = i % totalRows;
				output.Append(sortedColChars[currentRow, currentColumn]);
			}

			return output.ToString();
		}

		public static string Decipher(string input, string key)
		{
			StringBuilder output = new StringBuilder();
			int totalChars = input.Length;
			int totalColumns = (int)Math.Ceiling((double)totalChars / key.Length);
			int totalRows = key.Length;
			char[,] rowChars = new char[totalRows, totalColumns];
			char[,] colChars = new char[totalColumns, totalRows];
			char[,] unsortedColChars = new char[totalColumns, totalRows];
			int currentRow, currentColumn, i, j;
			int[] shiftIndexes = GetShiftIndexes(key);

			for (i = 0; i < totalChars; ++i)
			{
				currentRow = i / totalColumns;
				currentColumn = i % totalColumns;
				rowChars[currentRow, currentColumn] = input[i];
			}

			for (i = 0; i < totalRows; ++i)
				for (j = 0; j < totalColumns; ++j)
					colChars[j, i] = rowChars[i, j];

			for (i = 0; i < totalColumns; ++i)
				for (j = 0; j < totalRows; ++j)
					unsortedColChars[i, j] = colChars[i, shiftIndexes[j]];

			for (i = 0; i < totalChars; ++i)
			{
				currentRow = i / totalRows;
				currentColumn = i % totalRows;
				output.Append(unsortedColChars[currentRow, currentColumn]);
			}

			return output.ToString();
		}

        protected void Button1_Click(object sender, EventArgs e)
        {

			if (opcion.SelectedValue == "1")
			{
				string text = contenido.Value;
				string key = clave.Value;
				string cipherText = Encipher(text, key, '-');
				resultado.Value = cipherText;
				Down.Visible = true;
			}
			else if (opcion.SelectedValue == "2")
			{
				string ruta = Server.MapPath("~/file/" + archivo.PostedFile.FileName);
				archivo.PostedFile.SaveAs(ruta);

				string text = File.ReadAllText(Server.MapPath(@"~/file/" + archivo.PostedFile.FileName));
				string key = clave.Value;
				string plainText = Decipher(text, key);
				resultado.Value = plainText.Replace("-","");
			}
		}

        protected void Down_Click(object sender, EventArgs e)
        {
			Response.Clear();
			Response.Buffer = true;
			Response.AddHeader("content-disposition", "attachment;filename=MensajeCajaEncriptado.txt");
			Response.Charset = "";
			Response.ContentType = "application/text";
			Response.Output.Write(resultado.Value);
			Response.Flush();
			Response.End();
		}

        protected void opcion_SelectedIndexChanged(object sender, EventArgs e)
        {
			if (opcion.SelectedValue == "1")
            {
				archivo.Visible = false;
				contenido.Visible = true;
				resultado.Value = "";
            }
            else if (opcion.SelectedValue == "2")
			{
				archivo.Visible = true;
				contenido.Visible = false;
				Down.Visible = false;
				resultado.Value = "";
			}
        }
    }
}