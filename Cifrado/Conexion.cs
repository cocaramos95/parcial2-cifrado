﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WsFilesCloud
{
    public class Conexion
    {
        public static DataTable RunSql(string Sql)
        {
            using (SqlConnection Cx = new SqlConnection(ConfigurationManager.ConnectionStrings["SRVUMG"].ConnectionString))
            {
                SqlCommand Command = new SqlCommand(Sql, Cx);
                DataTable Datos = new DataTable();
                Cx.Open();
                Command.CommandTimeout = 0;
                SqlDataReader Reader = Command.ExecuteReader();
                Datos.Load(Reader);
                Cx.Close();
                Command.Dispose();
                return Datos;
            }
        }
        public static bool InsertDql(string Sql)
        {
            using (SqlConnection Cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SRVUMG"].ConnectionString))
            {
                SqlCommand Command = new SqlCommand(Sql, Cnn);
                Cnn.Open();
                Command.CommandTimeout = 0;
                int RowsAffected = Command.ExecuteNonQuery();
                if (RowsAffected > 0)
                    return true;
                else
                    return false;
            }
        }
        public static bool UpdateSql(string Sql)
        {
            using (SqlConnection Cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SRVUMG"].ConnectionString))
            {
                SqlCommand Command = new SqlCommand(Sql, Cnn);
                Cnn.Open();
                Command.CommandTimeout = 0;
                int RowsAffected = Command.ExecuteNonQuery();
                if (RowsAffected > 0)
                    return true;
                else
                    return false;
            }
        }
        public static bool DeleteSql(string Sql)
        {
            using (SqlConnection Cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SRVUMG"].ConnectionString))
            {
                SqlCommand Command = new SqlCommand(Sql, Cnn);
                Cnn.Open();
                Command.CommandTimeout = 0;
                int RowsAffected = Command.ExecuteNonQuery();
                if (RowsAffected > 0)
                {
                    Cnn.Close();
                    return true;
                }
                else
                {
                    Cnn.Close();
                    return false;
                }
            }
        }
        public static string RegSql(string Sql)
        {
            try
            {
                using (SqlConnection Cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SRVUMG"].ConnectionString))
                {
                    SqlCommand Command = new SqlCommand(Sql, Cnn);
                    Cnn.Open();
                    Command.CommandTimeout = 0;
                    return Convert.ToString(Command.ExecuteScalar());
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        public static bool ExistIDSql(string Sql)
        {
            using (SqlConnection Cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SRVUMG"].ConnectionString))
            {
                SqlCommand Command = new SqlCommand(Sql, Cnn);
                Cnn.Open();
                Command.CommandTimeout = 0;
                SqlDataReader Reader = Command.ExecuteReader();
                if (Reader.Read())
                    return true;
                else
                    return false;
            }
        }
        public static int MaxIDSql(string Sql)
        {
            using (SqlConnection Cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["SRVUMG"].ConnectionString))
            {
                SqlCommand Command = new SqlCommand(Sql, Cnn);
                Command.CommandTimeout = 0;
                Cnn.Open();
                return Convert.ToInt32(Command.ExecuteScalar());
            }
        }
    }
}