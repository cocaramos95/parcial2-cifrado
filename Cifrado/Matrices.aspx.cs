﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsFilesCloud;

namespace Cifrado
{
    public partial class Matrices : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Getclave();
        }

        private void Getclave()
        {
            string Sql = "SELECT * FROM CLAVES";
            DataTable dt = new DataTable();
            dt = Conexion.RunSql(Sql);

            foreach (DataRow Row in dt.Rows)
            {
                m11.Value = Row["m11"].ToString();
                m12.Value = Row["m12"].ToString();
                m13.Value = Row["m13"].ToString();
                m21.Value = Row["m21"].ToString();
                m22.Value = Row["m22"].ToString();
                m23.Value = Row["m23"].ToString();
                m31.Value = Row["m31"].ToString();
                m32.Value = Row["m32"].ToString();
                m33.Value = Row["m33"].ToString();
            }

         }

        static int[,] ConvertMatrix(int[] flat, int m, int n)
        {
            if (flat.Length != m * n)
            {
                throw new ArgumentException("Invalid length");
            }
            int[,] ret = new int[m, n];

            for (int i = 0; i < flat.Length; i++)
            {
                ret[i % m, i / m] = flat[i];
            }
            return ret;
        }

        static int[,] ConvertMatriz(int[] flat, int m, int n)
        {
            if (flat.Length != m * n)
            {
                throw new ArgumentException("Invalid length");
            }
            int[,] ret = new int[m, n];

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    ret[i, j] = flat[i * n + j];
                }
            }

            return ret;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string[] letras = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", " " };

            int[,] matriz_clave = { { Convert.ToInt32(m11.Value), Convert.ToInt32(m12.Value), Convert.ToInt32(m13.Value) }, { Convert.ToInt32(m21.Value), Convert.ToInt32(m22.Value), Convert.ToInt32(m23.Value) }, { Convert.ToInt32(m31.Value), Convert.ToInt32(m32.Value), Convert.ToInt32(m33.Value) } };

            if (opcion.SelectedValue == "1")
            {
                int tamano = contenido.Value.Length;
                string cadena = contenido.Value.ToUpper();

                int numero = tamano % 3;

                if (tamano % 3 != 0)
                {
                    while (numero != 0)
                    {
                        cadena += " ";
                        numero = cadena.Length % 3;
                    }
                }

                char[] array = cadena.ToCharArray();
                List<int> termsLetter = new List<int>();

                for (int i = 0; i < array.Length; i++)
                {
                    char letter = array[i];

                    int index = Array.IndexOf(letras, Convert.ToString(letter));

                    termsLetter.Add(index + 1);
                }

                int[] terms = termsLetter.ToArray();

                int[,] matriz_mensaje = ConvertMatrix(terms, 3, cadena.Length / 3);

                int[][] result = MatrixProduct(matriz_clave, matriz_mensaje, cadena.Length / 3);

                string cifrado = "";

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < cadena.Length / 3; j++)
                    {
                        cifrado += result[i][j] + ",";
                    }

                }

                cifrado = cifrado.TrimEnd(',');
                resultado.Value = cifrado;
                Down.Visible = true;
            }
            else if (opcion.SelectedValue == "2")
            {
                string ruta = Server.MapPath("~/file/" + archivo.PostedFile.FileName);
                archivo.PostedFile.SaveAs(ruta);

                string text = File.ReadAllText(Server.MapPath(@"~/file/" + archivo.PostedFile.FileName));

                string[] split = text.Split(',');
                List<int> termsLetter = new List<int>();

                foreach (string item in split)
                {
                    termsLetter.Add(Convert.ToInt32(item));
                }

                int[] terms = termsLetter.ToArray();

                int[,] matriz_mensaje = ConvertMatriz(terms, 3, split.Length / 3);

                int[,] adj = new int[3, 3];

                float[,] inv = new float[3, 3]; 


                Console.Write("Input matrix is :\n");
                display(matriz_clave);

                Console.Write("\nThe Adjoint is :\n");
                adjoint(matriz_clave, adj);
                display(adj);

                if (inverse(matriz_clave, inv))
                    display(inv);

                int[][] result = MatrixProduct(adj, matriz_mensaje, split.Length / 3);
                List<int> msg = new List<int>();

                string descifrado = "";
                string traduccion = "";

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < split.Length / 3; j++)
                    {
                        descifrado += result[i][j] + ",";
                    }
                }

                for (int k = 0; k < split.Length/3; k++)
                {
                    for (int l = 0; l < 3; l++)
                    {
                        msg.Add(result[l][k]);
                    }
                }

                int[] termsmsg = msg.ToArray();

                for (int x =0; x < termsmsg.Length; x++)
                {
                    traduccion += letras[termsmsg[x] - 1];
                }

                descifrado = descifrado.TrimEnd(',');
                resultado.Value = descifrado + "\n" + traduccion;

            }
        }

        static void getCofactor(int[,] A, int[,] temp, int p, int q, int n)
        {
            int i = 0, j = 0;

            for (int row = 0; row < n; row++)
            {
                for (int col = 0; col < n; col++)
                {
                    if (row != p && col != q)
                    {
                        temp[i, j++] = A[row, col];

                        if (j == n - 1)
                        {
                            j = 0;
                            i++;
                        }
                    }
                }
            }
        }

        static int determinant(int[,] A, int n)
        {
            int D = 0; 

            if (n == 1)
                return A[0, 0];

            int[,] temp = new int[3, 3];  

            int sign = 1;

            for (int f = 0; f < n; f++)
            {
                
                getCofactor(A, temp, 0, f, n);
                D += sign * A[0, f] * determinant(temp, n - 1);

                sign = -sign;
            }
            return D;
        }

        static void adjoint(int[,] A, int[,] adj)
        {
            if (3 == 1)
            {
                adj[0, 0] = 1;
                return;
            }

            
            int sign = 1;
            int[,] temp = new int[3, 3];

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    getCofactor(A, temp, i, j, 3);

                    sign = ((i + j) % 2 == 0) ? 1 : -1;

                    adj[j, i] = (sign) * (determinant(temp, 3 - 1));
                }
            }
        }

        static bool inverse(int[,] A, float[,] inverse)
        {
            
            int det = determinant(A, 3);
            if (det == 0)
            {
                Console.Write("Singular matrix, can't find its inverse");
                return false;
            }
 
            int[,] adj = new int[3, 3];
            adjoint(A, adj);

            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    inverse[i, j] = adj[i, j] / (float)det;

            return true;
        }

        static void display(int[,] A)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                    Console.Write(A[i, j] + " ");
                Console.WriteLine();
            }
        }
        static void display(float[,] A)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                    Console.Write("{0:F6} ", A[i, j]);
                Console.WriteLine();
            }
        }



        public static int[][] MatrixCreate(int rows, int cols)
        {
             
            int[][] result = new int[rows][];
            for (int i = 0; i < rows; ++i)
                result[i] = new int[cols];
            return result;
        }

        public static int[][] MatrixProduct(int[,] matrixA, int[,] matrixB, int columnsb)
        {
            int aRows = 3; int aCols = 3;
            int bRows = 3; int bCols = columnsb;
            if (aCols != bRows)
                throw new Exception("Non-conformable matrices in MatrixProduct");
            int[][] result = MatrixCreate(aRows, bCols);
            for (int i = 0; i < aRows; ++i) 
                for (int j = 0; j < bCols; ++j) 
                    for (int k = 0; k < aCols; ++k)
                        result[i][j] += matrixA[i, k] * matrixB[k, j];
            return result;
        }

        protected void Down_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=MensajeMatrizEncriptado.txt");
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(resultado.Value);
            Response.Flush();
            Response.End();
        }

        protected void opcion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (opcion.SelectedValue == "1")
            {
                archivo.Visible = false;
                contenido.Visible = true;
                resultado.Value = "";
            }
            else if (opcion.SelectedValue == "2")
            {
                archivo.Visible = true;
                contenido.Visible = false;
                Down.Visible = false;
                resultado.Value = "";
            }
        }
    }
}