﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Matrices.aspx.cs" Inherits="Cifrado.Matrices" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>MATRICES</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</head>
<body>
    <div class="content">
        <div class="container-fluid">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="Cajas.aspx">METODO CAJAS</a>
                        <a class="navbar-brand" href="Matrices.aspx"><b style="color:red">METODO MATRICES</b></a>
                        <a class="navbar-brand" href="Claves.aspx">CLAVES</a>
                    </div>
                </div>
            </nav>
            <form id="form1" runat="server">
                <div class="row">
                    <div class="col-md-4">
                        <div class="col-md-12">
                            <asp:RadioButtonList ID="opcion"  RepeatDirection="Horizontal" OnSelectedIndexChanged="opcion_SelectedIndexChanged"
                               Width="100%" runat="server" RepeatColumns="5" RepeatLayout="Table" AutoPostBack="true" required="required">
                                <asp:ListItem Value="1" Selected="True" Text="Encriptar"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Desencriptar"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <br />
                        <br />
                        <div class="col-md-12">
                            <asp:Label ID="Label1" runat="server" Text="Mensaje"></asp:Label>
                            <br />
                            <input type="text" class="form-control" runat="server" id="contenido" required="required" />
                            <input type="file" runat="server" id="archivo" visible="false" class="form-control" />
                        </div>
                        <br />
                        <br />
                        <div class="col-md-12">
                            <asp:Label ID="Label2" runat="server" Text="Clave:"></asp:Label>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m11" readonly="true" required="required" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m12" readonly="true" required="required" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m13" readonly="true" required="required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m21" readonly="true" required="required" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m22" readonly="true" required="required" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m23" readonly="true" required="required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m31" readonly="true" required="required" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m32" readonly="true" required="required" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m33" readonly="true" required="required" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br />
                        <br />
                        <div class="col-md-12">
                            <asp:Label ID="Label3" runat="server" Text="Resultado"></asp:Label>
                            <br />
                            <textarea id="resultado" runat="server" readonly="readonly" cols="60" rows="3"></textarea>
                        </div>
                        <br />
                        <br />
                        <div class="col-md-6">
                            <asp:Button ID="Button1" Width="95%" runat="server" CssClass="btn-success btn" Text="Procesar" OnClick="Button1_Click"  />
                        </div>
                        <div class="col-md-6">
                            <asp:Button ID="Down" Width="95%" runat="server" Visible="false" CssClass="btn-info btn" Text="Descargar" OnClick="Down_Click" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
