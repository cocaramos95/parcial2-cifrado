﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsFilesCloud;

namespace Cifrado
{
    public partial class Claves : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                Getclave();
            }            
        }

        private void Getclave()
        {
            string Sql = "SELECT * FROM CLAVES";
            DataTable dt = new DataTable();
            dt = Conexion.RunSql(Sql);

            foreach (DataRow Row in dt.Rows)
            {

                contenido.Value = Row["caja"].ToString();

                m11.Value = Row["m11"].ToString();
                m12.Value = Row["m12"].ToString();
                m13.Value = Row["m13"].ToString();
                m21.Value = Row["m21"].ToString();
                m22.Value = Row["m22"].ToString();
                m23.Value = Row["m23"].ToString();
                m31.Value = Row["m31"].ToString();
                m32.Value = Row["m32"].ToString();
                m33.Value = Row["m33"].ToString();
            }

        }

        protected void btn_upd_Click(object sender, EventArgs e)
        {
            string Sql = "UPDATE CLAVES SET caja='" + contenido.Value + "'" +
                ", m11='" + m11.Value + "'" +
                ", m12='" + m12.Value + "'" +
                ", m13='" + m13.Value + "'" +
                ", m21='" + m21.Value + "'" +
                ", m22='" + m22.Value + "'" +
                ", m23='" + m23.Value + "'" +
                ", m31='" + m31.Value + "'" +
                ", m32='" + m32.Value + "'" +
                ", m33='" + m33.Value + "'";
            Conexion.UpdateSql(Sql);
        }
    }
}