﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Claves.aspx.cs" Inherits="Cifrado.Claves" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>CAJAS</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</head>
<body>
    <div class="content">
        <div class="container-fluid">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="Cajas.aspx">METODO CAJAS</a>
                        <a class="navbar-brand" href="Matrices.aspx">METODO MATRICES</a>
                        <a class="navbar-brand" href="Claves.aspx"><b style="color:red">CLAVES</b></a>
                    </div>
                </div>
            </nav>
            <form id="form1" runat="server">
                <div class="row">
                    <div class="col-md-4">
                        <div class="col-md-12">
                            <asp:Label ID="Label1" runat="server" Text="Llave de cajas"></asp:Label>
                            <br />
                            <input type="text" class="form-control" runat="server" id="contenido" required="required" />
                        </div>
                        <br />
                        <br />
                        <div class="col-md-12">
                            <br />
                        <br />
                            <asp:Label ID="Label2" runat="server" Text="Llave de matriz:"></asp:Label>
                            <br />
                            <table runat="server">
                                <tr>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m11" required="required" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m12" required="required" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m13" required="required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m21"  required="required" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m22"  required="required" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m23" required="required" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m31" required="required" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m32" required="required" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" runat="server" id="m33" required="required" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <br />
                            <asp:Button ID="btn_upd" Width="95%" runat="server" CssClass="btn-success btn" Text="Actualizar" OnClick="btn_upd_Click" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
